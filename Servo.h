#include "fsl_gpio.h"
#include "Delay.h"

// ::: DEFINES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#define Servo_Pin  		16
#define Servo_Output	GPIO_PinInit  (GPIO, 0, Servo_Pin, &out) // Inicializo el pin del puerto 0 como salida (PIN del Servo)

#define Servo_High  	GPIO_PinWrite(GPIO, 0, Servo_Pin, 1)
#define Servo_Low		GPIO_PinWrite(GPIO, 0, Servo_Pin, 0)



// ::: PROTOTIPOS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//	Prototipos de las funciones que realizan el movimiento del Servo
	void Angulo(int angulo);
	void Mover(int tiempo);
	void Inicio_Servo(void);

