#include "Delay.h"

//::: VARIABLES :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
volatile uint32_t count_useg = 0;				// Variable del Timer
volatile uint32_t count_mseg = 0; 				// Variable del Systick


//:::: Callback para c/ canal ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	ctimer_callback_t ctimer_callback_table[] = { ctimer_match0_callback, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

	static ctimer_match_config_t matchConfig0;		// Estructura para la configuración del canal 0



//::: FUNCIONES :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

/* Función de Configuración del CTIMER * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	void CTIMER_config(void)
	{
			ctimer_config_t config;

			CTIMER_StopTimer(CTIMER0);

			CTIMER_GetDefaultConfig(&config);
			CTIMER_Init(CTIMER0, &config);

			//Configuro canal 0
			matchConfig0.enableCounterReset = true;
			matchConfig0.enableCounterStop  = false;
			matchConfig0.matchValue         = SystemCoreClock/10000; 			// Cada 100uS
			matchConfig0.outControl         = kCTIMER_Output_NoAction;
			matchConfig0.outPinInitState    = false;
			matchConfig0.enableInterrupt    = true;

			CTIMER_RegisterCallBack(CTIMER0, &ctimer_callback_table[0], kCTIMER_MultipleCallback);
			CTIMER_SetupMatch(CTIMER0, kCTIMER_Match_0, &matchConfig0);

			CTIMER_StartTimer(CTIMER0);
	}


/* Función de Delay en ms  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																												 			 *
 * parametro: 																									 			 *
 * 		uint32_t ms													 											 			 *
 * 																												 			 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	void delay_ms(uint32_t ms)
	{
		count_mseg = ms;
		while(count_mseg != 0);
	}

/* Función de Delay en us  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 																															 *
 * parametro: 																									 			 *
 * 		uint32_t us													 											 			 *
 * 																												 			 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	void delay_us(uint32_t us)
	{
		count_useg = us;
		while(count_useg != 0);
	}

// ::: INTERRUPCIONES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/* Timer * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 	void ctimer_match0_callback(uint32_t flags)
	{
		if (count_useg != 0)
			count_useg--;		// La variable se decrementará cada vez que ingrese a la interrupcion
	}


/* Systick * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	void SysTick_Handler(void)
	{
		if (count_mseg != 0)
			count_mseg--;
	}

