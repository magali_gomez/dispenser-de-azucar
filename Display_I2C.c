#include "Display_I2C.h"

// ::: FUNCIONES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/* Función de Configuración de I2C * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * */
	void configuracionI2C(void)
	{
		/* Attach main clock to I2C */
			CLOCK_Select(kI2C1_Clk_From_MainClk);								// Especifico la Fuente de CLK
			CLOCK_EnableClock(kCLOCK_Swm);										// Selecciono la Matriz de Conmutación

			/* Configuracion de los Pines IO para el I2C */
			SWM_SetMovablePinSelect(SWM0, kSWM_I2C1_SCL, SCL_Pin);
			SWM_SetMovablePinSelect(SWM0, kSWM_I2C1_SDA, SDA_Pin);

			CLOCK_DisableClock(kCLOCK_Swm);										// Desactivo el CLK de la Matriz de Conmutacion

		// CONFIGURACION DEL UC COMO MASTER
			i2c_master_config_t masterConfig;

			/* Get default configuration for master. */
			/* La Variable queda configurada de esta manera
			 * masterConfig.debugEnable = false;
			 * masterConfig.ignoreAck = false;
			 * masterConfig.pinConfig = kI2C_2PinOpenDrain;
			 * masterConfig.baudRate_Bps = 100000U;
			 * masterConfig.busIdleTimeout_ns = 0;
			 * masterConfig.pinLowTimeout_ns = 0;
			 * masterConfig.sdaGlitchFilterWidth_ns = 0;
			 * masterConfig.sclGlitchFilterWidth_ns = 0;		*/
				I2C_MasterGetDefaultConfig(&masterConfig);
					masterConfig.baudRate_Bps = baudRate;					// Configuro el BaudRate

			/* Init I2C master. */
				I2C_MasterInit(I2C1, &masterConfig, Frecuency);
	}
