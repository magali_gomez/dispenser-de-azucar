#include <fsl_ctimer.h>

//::: PROTOTIPOS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void delay_us(uint32_t us);						// Prototipo de la Función de delay en microsegundos
void delay_ms(uint32_t ms);						// Prototipo de la función de delay en milisegundos
void CTIMER_config(void);
void ctimer_match0_callback(uint32_t flags); 	// Callback canal 0




