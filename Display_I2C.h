#include "fsl_SSD1306.h"
#include "fsl_i2c.h"
#include "fsl_iocon.h"
#include "fsl_swm.h"

// ::: DEFINES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#define Frecuency 		CLOCK_GetFreq(kCLOCK_MainClk)
#define baudRate 		400000
#define SCL_Pin			20
#define SDA_Pin			18

// ::: PROTOTIPO ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void configuracionI2C(void);

