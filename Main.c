/*:::::::::::::::::::::::::::::::::::::::::::: Código Principal :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 *:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

#include "Delay.h"
#include "Servo.h"
#include "Botones.h"
#include "Mensajes.h"
#include "Display_I2C.h"


// ::: DEFINES :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#define GPIO_0			GPIO_PortInit (GPIO, 0)			// Inicializo el oscilador del GPIO 0


int main(void)
{

//:::: Variables :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	int i = 1;
	int mas, menos, ok;

//::::::::::SET UP :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// Configuracion del CTIMER;
		CTIMER_config();

	// Declaración de las estructuras de salida y entrada
		gpio_pin_config_t out = {kGPIO_DigitalOutput,1};
		gpio_pin_config_t in  = {kGPIO_DigitalInput};

	// Configuración del Systick
	(void) SysTick_Config(SystemCoreClock/1000); // Entra a la Interrupcion cada 1ms

	// Configuración del I2C
		configuracionI2C();

	// Inicialización del CLK del GPIO0
		GPIO_0;

	// Inicialización de los pines como salidas o entradas
		Servo_Output;
		MAS_Input;
		MENOS_Input;
		OK_Input;

	// Inicialización del Display
		OLED_Init();
		OLED_Clear();
		OLED_Refresh();

	// Inicialización del Servo
		Inicio_Servo();

	//-- MENSAJES DE INICIO	----------------------------------------------------------------------------------------------------------

		Hola();

		Seleccione();

		Cucharita_1();

//:::::::::: LOOP ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	while(1) {

	// LECTURA DE LOS BOTONES
		mas = GPIO_PinRead(GPIO, 0, MAS);			// Lectura del Boton [+]

		menos = GPIO_PinRead(GPIO, 0, MENOS);		// Lectura del Boton [-]

		ok = GPIO_PinRead(GPIO, 0, OK);				// Lectura del Boton [OK]



		if (mas == 1 || menos == 1)					// Cuando se presione el boton [+] o [-]
		{
			delay_ms(400);							// Se espera para que no se lea inmediatamente de nuevo el boton

			if (mas == 1)							// Si se apreto [+] se incrementa i
				i = i + 1;

			if (menos == 1)							// Si se apreto [-] se decrementa i
				i = i - 1;

			if (i >= 5)								// Si ya se llego al máximo se queda ahí
			{
				i = 5;
				Cucharita_5();
			}

			if (i <= 1)								// Si se llego al mínimo que se queda ahí
			{
				i = 1;
				Cucharita_1();
			}

			else
			{
				switch (i)
				{
					case 2:
						Cucharita_2();
						break;

					case 3:
						Cucharita_3();
						break;

					case 4:
						Cucharita_4();
						break;

				}// Fin del Switch

			}// Fin else

		}// Fin if (mas == 1 || menos == 1)

		if (ok == 1)						// Si se pulso [OK]
		{
			switch (i)
				{
					case 1:
							Servir();
							Mover(500);
							break;

					case 2:
							Servir();
							Mover(1000);
							break;

					case 3:
							Servir();
							Mover(1500);
							break;

					case 4:
							Servir();
							Mover(2000);
							break;

					case 5:
							Servir();
							Mover(2500);
							break;
				} // Fin del Switch

			i = 1;					// Se resetea el valor de la variable

			Ready();				// Indica que se termino de Servir

			Seleccione();			// Muestra mensaje de Selección

			Cucharita_1();			// Se muestra nuevamente la imagen de la 1er Cucharita

		} // Fin del if (ok == 1)

	} // Fin while(1)

	return 0 ;
}




