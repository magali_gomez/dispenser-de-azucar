#include "Mensajes.h"

/* Funciones de Mensajes * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * */
	void Hola (void)
	{
		OLED_Copy_Image(&hola[0], sizeof(hola));		// Hola + Carita Feliz
		OLED_Refresh();
		delay_ms(2000);
	}

	void Seleccione (void)
	{
		OLED_Copy_Image(&Select[0], sizeof(Select));	// Seleccione Cantidad de Azúcar
		OLED_Refresh();
		delay_ms(1000);

	}
	void Servir (void)
	{
		for (int m = 1; m < 3; m++)
		{
			OLED_Copy_Image(&OFF[0], sizeof(OFF));
			OLED_Refresh();
			delay_ms(400);
			OLED_Copy_Image(&ON[0], sizeof(ON));
			OLED_Refresh();
			delay_ms(400);
		}
	}

	void Ready (void)
	{
		OLED_Copy_Image(&bien[0], sizeof(bien));
		OLED_Refresh();
		delay_ms(1000);
	}

/* Funciones de Cucharitas * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * * * * * */
	 void Cucharita_1(void)
	 {
		OLED_Copy_Image(&Cucharita1[0], sizeof(Cucharita1));
		OLED_Refresh();
	 }

	 void Cucharita_2(void)
	 {
		OLED_Copy_Image(&Cucharita2[0], sizeof(Cucharita2));
		OLED_Refresh();
	 }

	 void Cucharita_3(void)
	 {
		OLED_Copy_Image(&Cucharita3[0], sizeof(Cucharita3));
		OLED_Refresh();
	 }

	 void Cucharita_4(void)
	 {
		OLED_Copy_Image(&Cucharita4[0], sizeof(Cucharita4));
		OLED_Refresh();
	 }

	 void Cucharita_5(void)
	 {
		OLED_Copy_Image(&Cucharita5[0], sizeof(Cucharita5));
		OLED_Refresh();
	 }
