#include "Servo.h"

// ::: VARIABLES ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	int high = 0;									// Variable de la Función Servo

/* Función de Ángulo de movimiento del Servo * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *																															 *
 * 		parámetro = angulo al que se quiere mover el servo																	 *
 *																															 *
 *		a partir de la ecuación de la recta que describe el funcionamiento del servo, se halla el valor en tiempo 			 *
 *		en el que debe permanecer la salida del uC en alto enviando un 1 al pin de PWM del servo, luego se manda un 0 		 *
 *																															 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	void Angulo(int angulo)
	{
		high = 15 - (angulo/9);			// Ecuación de la Recta de Tiempo en Función del Ángulo
		Servo_High;						// Manda un 1 al pin del servo
		delay_us(high);					// Aplica un delay segun el tiempo que debe estar en alto
		Servo_Low;						// Manda un 0 al pin del servo
	}

/* Función de movimiento del Servo * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *																															 *
 * 		parámetro = tiempo en ms para el cual se quiere mantener abierta la salida											 *
 *																															 *																													 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	void Mover(int tiempo_ms)
	{
		Angulo(10);
		delay_ms(tiempo_ms);
		Angulo(80);
	}

/* Función de Inicio del Servo * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *		Es necesaria para que el servo funcione correctamente, caso contrario el programa no iniciaba 					 *																															 *																													 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	void Inicio_Servo(void)
	{
		for (int g = 0; g < 3; g++)
			Mover(1);
	}
