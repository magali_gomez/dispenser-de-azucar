// Botones de Selección
#define MAS				27
#define MENOS			29
#define OK				22

#define MAS_Input 		GPIO_PinInit  (GPIO, 0, MAS, &in)
#define MENOS_Input 	GPIO_PinInit  (GPIO, 0, MENOS, &in)
#define OK_Input		GPIO_PinInit  (GPIO, 0, OK, &in)
